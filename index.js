// console.log("Send Stars")

// [SECTION] Exponent Operator
	//Before ES6 
	const firstNum = 8 ** 2
	console.log(firstNum);

	// ES6 
	/*
		Syntax: Math.pow(base, exponent);
	*/
	const secondNum = Math.pow(8,2);
	console.log(secondNum);



// [SECTION] Template Literals
	// allows us to write strings without using concatenation operator (+).
	/*Syntax*/

	let name = "John";
		// Before ES6
		let message = "Hello" + name + "! Welcome to programming."
		console.log(message);

		// ES6 
		// uses backticks (``)
		message = `Hello ${name} ! Welcome to programming.`
		console.log(message);

		const interestRate =0.1;
		const principal =1000;
		console.log(`The interest on your savings account is: ${interestRate*principal}.`)



// [SECTION] Array Destructuring
	// allows us to unpack elements in an array into distinct variables
	// allows us to name array elements with variables instead of using index number

		/*
		Syntax:
			let/const [varA, varB, ...] =
			arrayName;
		*/

	const fullName = ["Juan", "Dela","Cruz"];
		// Before ES6
		let firstName = fullName[0]
		let middleName = fullName[1]
		let lastName = fullName[2]

		console.log(`Hello ${firstName} ${middleName} ${lastName}`)

		// ES6
		const [fName,mName,lName] = fullName
		console.log(fName);
		console.log(mName);
		console.log(lName);

		// Mini Activity
		let array = [1,2,3,4,5];

		let [a,b,c,d,e] = array
		console.log(a);
		console.log(b);
		console.log(c);
		console.log(d);
		console.log(e);


// [SECTION] Object Destructuring
	// allows us to unpack properties of objects into distinct variables

		/*
		Syntax:
			let/const [propertyNameA, propertyNameB, ...] =
			objectName;
		*/

		const person = {
			givenName: "Jane",
			maidenName: "Dela",
			familyName: "Cruz"
		}

		// Before ES6

		let gName = person.givenName;
		let midName = person.maidenName;
		let famName = person.familyName;
		console.log(gName);
		console.log(midName);
		console.log(famName);

		// ES6
		let {givenName, maidenName,familyName} = person
		console.log("Object destructuring after ES6:")
		console.log(givenName);
		console.log(maidenName);
		console.log(familyName);


// [SECTION] Arrow Functions
	// compact alternative syntax to traditional functions

		/*
		Syntax:
		*/

		const hello = () =>{
			console.log("Hello World")
		}

		hello();

//function expression
		/*const hello = function(){
			console.log("Hello World")
		}*/
// function declaration

		/*function hello (){
			console.log("Hello World")
		}*/
		


// [SECTION] Implicit Return
	// works even without using "return" keyword

		console.log("Implicit Return")

		//Single
		const subtract1 = (x,y)=> x-y;
		console.log(subtract1(10,5))

		//Multi
		const subtract2 = (x,y)=> {
			return x-y;
		}
		console.log(subtract2(10,5))



// [SECTION] Default Function Argument Value 

		const greet = (firstName, lastName) => {
			return `Good afternoon, ${firstName="firstName "} ${lastName="lastName "}`
		}
		console.log(greet())


// [SECTION] Class-based Object Blueprints
	// allows us to create/instiation of objects using classed blueprints
	// create class
		// constructor is a special method of a class fro creating/initializing an object of the class

		/*
		Syntax:
		class className{
			constructor(objectValueA, objectValueB...){
				this.objectPropertyA = objectValueA;
				this.objectPropertyB = objectValueB
			}
		}
		*/

		class Car{
			constructor(brand,name,year){
				this.carBrand = brand;
				this.carName = name;
				this.carYear = year;
			}
		}

		let car = new Car ("Toyota", "Hilux-pickup",2015)
		console.log(car)

		car.carBrand = "Nissan"
		console.log(car);